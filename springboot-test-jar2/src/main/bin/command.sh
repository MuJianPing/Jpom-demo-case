# description: Auto-starts boot
export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
case "$(uname)" in
Linux)
  bin_abs_path=$(readlink -f $(dirname $0))
  ;;
*)
  bin_abs_path=$(
    cd $(dirname $0)
    pwd
  )
  ;;
esac

cygwin=false
linux=false
case "$(uname)" in
CYGWIN*)
  cygwin=true
  ;;
Linux*)
  linux=true
  ;;
esac

base=${bin_abs_path}/..

conf_path="${base}/conf"
Lib="${base}/lib/"
LogPath="${base}/logs/"
Log="${LogPath}/stdout.log"
logback_configurationFile="${conf_path}/logback.xml"
application_conf="${conf_path}/application.yml"

PID_TAG="test-jar-application-${PROJECT_ID}"
AllLog="${LogPath}/application.log"

#-Xms1g -Xmx2g
if [[ -z "${USR_JVM_SIZE}" ]]; then
  USR_JVM_SIZE="-Xms256m -Xmx1024m"
fi

JVM_ARGS="-server ${USR_JVM_SIZE} -XX:+UseG1GC -Dfile.encoding=UTF-8"
JVM_ARGS="${JVM_ARGS} -XX:MaxGCPauseMillis=250 -XX:+UseGCOverheadLimit -XX:+ExplicitGCInvokesConcurrent -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true"
JVM_ARGS="${JVM_ARGS} -Xss256k -XX:+AggressiveOpts -XX:-UseBiasedLocking -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=$LogPath"
JVM_ARGS="${JVM_ARGS} -XX:+UseFastAccessorMethods -XX:+PrintAdaptiveSizePolicy -XX:+PrintTenuringDistribution"
JVM_ARGS="${JVM_ARGS} -Dlogging.config=$logback_configurationFile -Dspring.config.location=$application_conf"
MAIN_ARGS="$*"

WAIT_LOG="$2"
RUN_JAR="${RUN_JAR}"

function checkConfig() {
  if [ ! -d "$LogPath" ]; then
    mkdir -p "$LogPath"
  fi
  if [[ ! -f "$logback_configurationFile" ]] || [[ ! -f "$application_conf" ]]; then
    echo "Cannot find $application_conf or $logback_configurationFile"
    exit 1
  fi
  if [[ -z "${RUN_JAR}" ]]; then
    RUN_JAR=$(ls -t "${Lib}" | grep '.jar$' | head -1)
    # error
    if [[ -z "${RUN_JAR}" ]]; then
      echo "Jar not found"
      exit 2
    fi
    echo "automatic running：${RUN_JAR}"
  fi
}

function getPid() {
  if $cygwin; then
    JAVA_CMD="$JAVA_HOME\bin\java"
    JAVA_CMD=$(cygpath --path --unix $JAVA_CMD)
    JAVA_PID=$(ps | grep $JAVA_CMD | awk '{print $1}')
  else
    if $linux; then
      JAVA_PID=$(ps -C java -f --width 1000 | grep "$PID_TAG" | grep -v grep | awk '{print $2}')
    else
      JAVA_PID=$(ps aux | grep "$PID_TAG" | grep -v grep | awk '{print $2}')
    fi
  fi
  echo $JAVA_PID
}

# See how we were called.
function start() {
  echo $PID_TAG
  checkConfig
  if [ ! -f $Log ]; then
    touch $Log
  fi
  # check running
  pid=$(getPid)
  #echo "$pid"
  if [ "$pid" != "" ]; then
    echo "Running, please do not run repeatedly:$pid"
    exit 1
  fi
  # start
  nohup java -Dappliction=${PID_TAG} ${JVM_ARGS} -jar ${Lib}${RUN_JAR} ${MAIN_ARGS} >$Log 2>&1 &
  if [[ ${WAIT_LOG} == "tail" ]]; then
    sleep 2s
    tail -f $AllLog
  fi
  echo "auto exit 0"
  sleep 1s
  exit 0
}

function stop() {
  pid=$(getPid)
  if [ "$pid" != "" ]; then
    echo -n "boot ( pid $pid) is running"
    echo
    echo -n $"Shutting down boot: "
    kill $pid

    LOOPS=0
    while (true); do
      pid=$(getPid)
      if [ "$pid" == "" ]; then
        echo "Stop and end, in $LOOPS seconds"
        break
      fi
      let LOOPS=LOOPS+1
      sleep 1
    done
  else
    echo "boot is stopped"
  fi
}

function status() {
  pid=$(getPid)
  #echo "$pid"
  if [ "$pid" != "" ]; then
    echo "running:$pid"
  else
    echo "boot is stopped"
  fi
}

function usage() {
  echo "Usage: $0 {start|stop|restart|status}"
  RETVAL="2"
}

# See how we were called.
RETVAL="0"
case "$1" in
start)
  start
  ;;
stop)
  stop
  ;;
restart)
  stop
  start
  ;;
status)
  status
  ;;
*)
  usage
  ;;
esac

exit $RETVAL
