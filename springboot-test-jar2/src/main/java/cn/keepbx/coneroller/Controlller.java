package cn.keepbx.coneroller;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.system.SystemUtil;
import cn.jiangzeyin.common.JsonMessage;
import cn.jiangzeyin.controller.base.AbstractController;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author jiangzeyin
 * @date 2018/8/21
 */
@RestController
public class Controlller extends AbstractController {

    private final static String UID = IdUtil.fastSimpleUUID();


    @RequestMapping(value = "/")
    public String test() {
        String clientIP = getClientIP();
        System.out.println(clientIP + " " + DateUtil.now() + "  " + UID);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("msg", "springBoot");
        jsonObject.put("ip", clientIP);
        String macAddress = NetUtil.getLocalMacAddress();
        jsonObject.put("pid", SystemUtil.getCurrentPID());
        jsonObject.put("macAddress", StrUtil.emptyToDefault(macAddress, "none"));
        jsonObject.put("time", DateUtil.now());
        jsonObject.put("UID", UID);
        return jsonObject.toString(SerializerFeature.PrettyFormat);
    }

    @RequestMapping(value = "/random-sleep", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String randomSleep(String max) {
        int maxInt = Convert.toInt(max, 1);
        int time = maxInt > 1 ? RandomUtil.randomInt(1, maxInt) : maxInt;
        ThreadUtil.sleep(time, TimeUnit.SECONDS);
        return JsonMessage.getString(200, "休眠了," + time + "秒", SystemUtil.getCurrentPID());
    }

    @RequestMapping(value = "/status")
    public String status() {
        return JsonMessage.getString(200, "running", SystemUtil.getCurrentPID());
    }
}
