package cn.keepbx.coneroller;

import cn.jiangzeyin.controller.base.AbstractController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jiangzeyin
 * @date 2018/8/21
 */
@RestController
public class Controller extends AbstractController {


    @RequestMapping(value = "/")
    public String test() {
        return "springBoot";
    }
}
