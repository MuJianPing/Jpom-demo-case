package cn.keepbx;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author jiangzeyin
 * @date 2019/4/18
 */
@Configuration
public class TestBean {
    @Value("${test.val:default}")
    public String test;
}
