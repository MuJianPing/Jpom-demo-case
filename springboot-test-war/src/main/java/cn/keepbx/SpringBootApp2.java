package cn.keepbx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author jiangzeyin
 * @date 2018/5/10
 */
@SpringBootApplication()
public class SpringBootApp2 extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        // 注意这里要指向原先用main方法执行的Application启动类
        return builder.sources(SpringBootApp2.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringBootApp2.class, args);
    }
}
